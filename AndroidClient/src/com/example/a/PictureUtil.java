package com.example.a;

import android.graphics.Bitmap;
import android.graphics.Matrix;
import org.apache.http.entity.mime.content.ByteArrayBody;

import java.io.ByteArrayOutputStream;

public class PictureUtil
{

    private static final int QUALITY = 100;

    public static ByteArrayBody toByteArrayBody( Bitmap picture ) {
        if ( picture == null ) {
            return null;
        }

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        picture.compress( Bitmap.CompressFormat.JPEG, QUALITY, bos );
        byte[] data = bos.toByteArray();
        return new ByteArrayBody( data, System.currentTimeMillis() + ".jpg" );
    }


    private static Bitmap resize( Bitmap picture, int targetWidth, int targetHeight ) {
        if ( picture == null || picture.getHeight() < 0 || picture.getWidth() < 0 ) {
            return null;
        }

        int pictureWidth = picture.getWidth();
        int pictureHeight = picture.getHeight();
        float scale = Math.min( ( float ) targetWidth / pictureWidth, ( float ) targetHeight / pictureHeight ); // (1)

        Matrix matrix = new Matrix();
        matrix.postScale( scale, scale );

        return Bitmap.createBitmap( picture, 0, 0, pictureWidth, pictureHeight, matrix, true );
    }
}
