package com.example.a;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.InputStreamBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.*;

public class MyActivity extends Activity
{
    private static final int REQUEST_CODE = 1;
    private static final int PICTURE_WIDTH = 640;

    private Uri mImageUri;

    @Override
    public void onCreate( Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.main );

        Button btn = ( Button ) findViewById( R.id.upload_btn );
        btn.setOnClickListener(
                new View.OnClickListener()
                {
                    @Override
                    public void onClick( View v ) {
                        Intent intent = new Intent( Intent.ACTION_PICK );
                        intent.setType( "image/*" );
                        startActivityForResult( intent, REQUEST_CODE );
                    }
                } );
    }


    @Override
    protected void onActivityResult( int requestCode, int resultCode, Intent data ) {
        super.onActivityResult( requestCode, resultCode, data );
        if ( requestCode == REQUEST_CODE && resultCode == RESULT_OK ) {
            mImageUri = data.getData();
            final Bitmap bmp = decodeUri( mImageUri, PICTURE_WIDTH );
            final ImageView iv = ( ImageView ) findViewById( R.id.image_view );
            iv.setImageBitmap( bmp );

            final Thread thread = new Thread( reqUpload );
            thread.start();
        }
    }

    private int calcSampleSize( final Uri uri, final int size ) {
        int sampleSize = 1;
        try {
            final InputStream is = getContentResolver().openInputStream( uri );

            final BitmapFactory.Options option = new BitmapFactory.Options();

            option.inJustDecodeBounds = true;

            BitmapFactory.decodeStream( is, null, option );
            is.close();

            sampleSize = option.outWidth / size;

        }
        catch ( FileNotFoundException e ) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        catch ( IOException e ) {
            e.printStackTrace();
        }
        return sampleSize;
    }

    private Bitmap decodeUri( final Uri uri, final int width ) {
        try {
            final int sampleSize = calcSampleSize( uri, width );
            BitmapFactory.Options opt = new BitmapFactory.Options();
            opt.inSampleSize = sampleSize;

            final InputStream is = getContentResolver().openInputStream( uri );
            Bitmap bmp = BitmapFactory.decodeStream( is, null, opt );
            is.close();
            return bmp;
        }
        catch ( FileNotFoundException e ) {
            e.printStackTrace();
        }
        catch ( IOException e ) {
            e.printStackTrace();
        }
        return null;
    }

    //画像をPOSTする
    private Runnable reqUpload = new Runnable()
    {
        @Override
        public void run() {
            final String postUrl = "http://cbdkyiu-avr-app000.c4sa.net/upload.php";
            final HttpClient httpClient = new DefaultHttpClient();
            final HttpPost post = new HttpPost(postUrl);
            final MultipartEntity entity = new MultipartEntity();

            try {
                final String path = getImagePath( mImageUri );
                final File file = new File(path);
                Log.d( "PATH",path );


                final Bitmap bmp = decodeUri( mImageUri, PICTURE_WIDTH );
                ByteArrayBody bab = PictureUtil.toByteArrayBody(bmp); // (2)
                MultipartEntity requestEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
//                entity.addPart( "image",new FileBody( file.getAbsoluteFile() ) );

                entity.addPart( "image",bab );

                post.setEntity( entity );

                final HttpResponse response = httpClient.execute(post);
                final int status = response.getStatusLine().getStatusCode();

                if ( status == HttpStatus.SC_OK ) {
                    Log.d("TEST","5");
                    final ByteArrayOutputStream oStream = new ByteArrayOutputStream();
                    response.getEntity().writeTo( oStream );
                    final String resp = oStream.toString();

                    Log.d( "SUCCESS",  resp  );
                }
            }catch(IOException e){
                e.printStackTrace();
            }

//            try {
//                String resp = null;
//                final String postUrl = "http://cbdkyiu-avr-app000.c4sa.net/upload.php";
//
//                InputStream inputStream = getContentResolver().openInputStream( mImageUri );
//                Log.d("TEST","1");
//
//
//                HttpClient httpClient = new DefaultHttpClient();
//                HttpPost post = new HttpPost( postUrl );
//                MultipartEntity entity = new MultipartEntity();
//
//                Log.d("TEST","2");
//                //InputStreamBody streamBody = new InputStreamBody( inputStream, "imagename" );
//                //entity.addPart( "image", streamBody );
//                //post.setEntity( entity );
//
//                Log.d("TEST","3");
//                HttpResponse response = httpClient.execute( post );
//                int status = response.getStatusLine().getStatusCode();
//
//                Log.d("TEST","4");
//                if ( status == HttpStatus.SC_OK ) {
//                    Log.d("TEST","5");
//                    ByteArrayOutputStream oStream = new ByteArrayOutputStream();
//                    response.getEntity().writeTo( oStream );
//                    resp = oStream.toString();
//
//                    Log.d( "SUCCESS",  resp  );
//                } else {
//                    Log.v( "ERR", "response status:" + String.valueOf( status ) );
//                }
//            }
//            catch ( IOException e ) {
//                Log.v( "ERR", "msg:" + e.getMessage() );
//            }
        }
    };

    private String getImagePath(final Uri uri){
        final String[] projection = {
                MediaStore.Images.Media.DATA
        };
        final Cursor cursor = MediaStore.Images.Media.query( getContentResolver(),uri,projection );
        cursor.moveToFirst();
        final String path = cursor.getString( cursor.getColumnIndex( MediaStore.Images.Media.DATA ) );
        cursor.close();
        return path;
    }

}
